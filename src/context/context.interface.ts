import { Context } from "telegraf";

export interface SessionData {
    getLocation: boolean;
    latitude: number;
    longitude: number;
    nextPage: number;
    showMorePage: number;
    currentCity: string;
    language: string
}

export interface IBotContext extends Context {
    session: SessionData;
}