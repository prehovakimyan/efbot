export const getNodesByCoordinatesScema: Object = `
  query {
    public {
    geoNodesByCoordinates(latitude: 46.63646 , longitude: 14.312225) {
      id
      name
      type
      iso
      distance
    }
  }
  }
`;