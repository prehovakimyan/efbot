import {IDictionaryItems} from "../Interfaces/IDictionaryItem";

var fs = require('fs');
import { Telegraf } from 'telegraf';
import * as schedule from "node-schedule";
import {getProfileForJobSchedule} from "../requests/getProfileForJobSchedule";
import {isCity} from "../middleware/isCity";
class job {

    constructor() {

    }
    //Shop code refresh at 12:00 AM
    public async shopRefresh() {

        const botCron = new Telegraf('6595964333:AAEbsnePGDN_FXXdSFZfNbBCeu9af8rxK1k');
        const allUsersFile = `${process.cwd()}/sessions.json`;

        schedule.scheduleJob("0 13 */7 * *", async function () {

            delete require.cache[require.resolve(allUsersFile)];
            const allUsers = fs.readFileSync(allUsersFile, 'utf8');
            const allUsersList: any = JSON.parse(allUsers);

            for (const item of allUsersList.sessions) {

                const checkCityName: any = await isCity(item.data.currentCity);

                if(checkCityName){
                    var message = await getProfileForJobSchedule(checkCityName,  0, item.data.language ? item.data.language : 'it');

                    if(message) {
                        botCron.telegram.sendPhoto(item.id, { url: message.msgBody.url }, {caption: message.msgBody.caption, parse_mode: "HTML", reply_markup: message.msgBody.keyboard });
                    }
                }
            }

        });
    }
}

export default new job();