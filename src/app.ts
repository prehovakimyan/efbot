import {ConfigService} from "./config/config.service";
import {IConfigService} from "./config/config.interface";
import { Telegraf } from "telegraf";
const LocalSession = require('telegraf-session-local');
import {IBotContext} from "./context/context.interface";
import {Command} from "./commands/command.class";
import {StartCommand} from "./commands/start.command";
import { ActionsCommands } from "./commands/actions.commands";
import { HelpCommand } from "./commands/help.command";
import {getDictionaries} from "./requests/getDictionaries";
import {getAllCities} from "./requests/getAllCities";
import job from './job/job';

class Bot {
    bot: Telegraf<IBotContext>;
    commands: Command[] = [];

    constructor(private readonly configService: IConfigService) {
        this.bot = new Telegraf<IBotContext>(this.configService.get('TOKEN'));
        this.bot.use(
            new LocalSession({ database: 'sessions.json' }).middleware()
        );
    }

    init() {
        this.commands = [new StartCommand(this.bot), new HelpCommand(this.bot) , new ActionsCommands(this.bot)];
        for (const command of this.commands){
            command.handle();
        }
        this.bot.launch();
        job.shopRefresh();
        this.bot.telegram.setWebhook('https://api.escortforumit.xxx/graphql').then(() => {

            new Promise<boolean>((res, rej) => {
                res(getDictionaries());
            })
                .then(res => {
                    getAllCities();
                    return false;
                })
                .catch(error => {
                    console.log('ERROR:', error.message);
                });


        }).catch((err) => {
            console.error('Error setting webhook:', err);
        });
    }

}

const bot = new Bot(new ConfigService());
bot.init();