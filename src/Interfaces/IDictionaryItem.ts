export interface IDictionaryItem {
    key: string,
    category: string,
    value_en: string,
    value_it: string,
    slug_en: string,
    slug_it: string
}

export interface IDictionaryItems extends Array<IDictionaryItem>{}

