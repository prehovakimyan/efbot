interface ICityItem {
    text: string
}

export default interface ICitiesRows extends Array<ICityItem>{}

