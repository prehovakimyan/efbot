import ICitiesRows from "../Interfaces/ICitiesRows";
interface ICitiesResultItems {
    resultRows: ICitiesRows,
    resultCities: Object
}



export default interface ICitiesResponse extends Array<ICitiesResultItems> {}

