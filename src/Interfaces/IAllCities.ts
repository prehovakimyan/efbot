interface IAllCitiesItems {
     city_id : number,
     type : string
     region_id : number
     region_name : string
     city_name : string
     profile_count : number
     distance_in_km : number
     city_name_en : string
     city_name_it : string
}



export default interface IAllCities extends Array<IAllCitiesItems> {}
