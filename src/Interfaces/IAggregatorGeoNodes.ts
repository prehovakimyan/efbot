import IAggregatorGeoCoordinates from "./IAggregatorGeoCoordinates";

export default interface IAggregatorGeoNodes {
    geo: { lat: number, lng: number},
    categories: { regular: boolean },
    filters: {},
    search: {},
}

