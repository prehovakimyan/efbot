
export default interface IAggregatorGeoCoordinates{
    id: number,
    name: string,
    iso: string,
    distance: number,
}