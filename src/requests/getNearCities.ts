import axios from 'axios';
import {getTranslation} from "../translations/translator";
import ICitiesRows from "../Interfaces/ICitiesRows";

export async function getNearCities(latitude: number, longitude: number, lang: string, ctx: any) {
    try {

        const query: Object = `query aggregatorGeoNodes {
                                                      public {
                                                        aggregatorGeoNodes(
                                                          geo: { type: CITY lat: ${latitude} , lng: ${longitude} }
                                                          categories: {
                                                            regular: true
                                                          }
                                                          filters: {}
                                                          search: {}
                                                        ) {
                                                          city_id
                                                          type
                                                          region_id
                                                          region_name
                                                          city_id
                                                          city_name
                                                          profile_count
                                                          distance_in_km
                                                        }
                                                      }
                                                    }
                                                    `;


        const response = await axios.post("https://api.escortforumit.xxx/graphql", {
            query: query,
        });

        const nearCities = response.data.data.public.aggregatorGeoNodes;

        nearCities.sort(function (a: any, b: any) {
            return a.distance_in_km - b.distance_in_km;
        });

        ctx.session.cities = nearCities;

        var resultRows = [[{ text: getTranslation(lang, 'current_location') }]];
        var resultRow: ICitiesRows = [];

        
        nearCities.forEach(( item: any, index: number ) => {
            if(index < 8) {
                if(index % 3 == 0 && index != 0) {
                    resultRows.push(resultRow);
                    resultRow = [];
                }
                resultRow.push({ text: `${getTranslation(lang, item.city_name)}` });
                if(index == 7) {
                    resultRow.push({ text: `${getTranslation(lang, 'more_cities')}` });
                    resultRows.push(resultRow);
                }
            }

        });

        resultRows.push([{ text: `${getTranslation(lang, 'change_language')}`}]);
        ctx.session.nextPage = 2;
        return resultRows;

    } catch (error) {
        return [];
    }
}

