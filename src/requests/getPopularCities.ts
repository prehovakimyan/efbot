import axios from 'axios';
import {getTranslation} from "../translations/translator";
import ICitiesRows from "../Interfaces/ICitiesRows";

export async function getPopularCities(lang: string, ctx: any) {
    try {

        const query: Object = `query aggregatorGeoNodes {
                                                      public {
                                                        aggregatorGeoNodes(
                                                          geo: { type: CITY }
                                                          categories: {
                                                            regular: true
                                                          }
                                                          filters: {}
                                                          search: {}
                                                        ) {
                                                          city_id
                                                          type
                                                          region_id
                                                          region_name
                                                          city_id
                                                          city_name
                                                          profile_count
                                                          distance_in_km
                                                        }
                                                      }
                                                    }
                                                    `;


        const response = await axios.post("https://api.escortforumit.xxx/graphql", {
            query: query,
        });

        const nearCities = response.data.data.public.aggregatorGeoNodes;


        nearCities.sort(function (a: any, b: any) {
            return b.profile_count - a.profile_count;
        });

        ctx.session.cities = nearCities;

        var resultRows: any = [[{text: `${getTranslation(lang, 'share_location_btn')}`, request_location: true}]];
        var resultRow: ICitiesRows = [];


        nearCities.forEach(( item: any, index: number ) => {
            if(index < 10) {
                if(index % 3 == 0 && index != 0) {
                    resultRows.push(resultRow);
                    resultRow = [];
                }
                resultRow.push({ text: `${getTranslation(lang, item.city_name)}` });
            }

        });

        resultRows.push([{ text: `${getTranslation(lang, 'change_language')}`}]);
        ctx.session.nextPage = 2;
        return resultRows;

    } catch (error) {
        return [];
    }
}

