import axios from 'axios';
import  IAggregatorGeoNodes from "../Interfaces/IAggregatorGeoNodes";
import {getTranslation} from "../translations/translator";
import ICitiesRows from "../Interfaces/ICitiesRows";

export async function getMoreCities(latitude: number, longitude: number, lang: string, ctx: any, page: number) {
    try {

        const nearCities = ctx.session.cities;
        const offset = page * 8;


        var resultRows = [[{ text: getTranslation(lang, 'current_location') }]];
        var resultRow: ICitiesRows = [];


        nearCities.forEach(( item: any, index: number ) => {
            if(index < offset) {
                if(index % 3 == 0 && index != 0) {
                    resultRows.push(resultRow);
                    resultRow = [];
                }
                resultRow.push({ text: item.city_name.charAt(0).toUpperCase() + item.city_name.slice(1) });
                if(index == (offset - 1)) {
                    resultRow.push({ text: `${getTranslation(lang, 'more_cities')}` });
                    resultRows.push(resultRow);
                }
            }

        });

        resultRows.push([{ text: `${getTranslation(lang, 'change_language')}`}]);

        ctx.session.nextPage = page + 1;
        return resultRows;

    } catch (error) {
        return [];
    }
}

