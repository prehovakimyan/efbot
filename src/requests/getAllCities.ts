import {ConfigService} from "../config/config.service";
import {IConfigService} from "../config/config.interface";
import axios from 'axios';
import {getTranslation} from "../translations/translator";
var fs = require('fs');

export async function getAllCities() {
    try {
        const configService: IConfigService = new ConfigService();

        configService.get('TOKEN');

        const query: Object = `query aggregatorGeoNodes {
                                                      public {
                                                        aggregatorGeoNodes(
                                                          geo: { type: CITY }
                                                          categories: {
                                                            regular: true
                                                          }
                                                          filters: {}
                                                          search: {}
                                                        ) {
                                                          city_id
                                                          type
                                                          region_id
                                                          region_name
                                                          city_id
                                                          city_name
                                                          profile_count
                                                          distance_in_km
                                                        }
                                                      }
                                                    }
                                                    `;


        const response = await axios.post("https://api.escortforumit.xxx/graphql", {
            query: query,
        });

        const allCities = response.data.data.public.aggregatorGeoNodes;

        const allCitiesArray = allCities.map((item: any) => {
            item.city_name_en = getTranslation("en", `${item.city_name}`).toLocaleLowerCase();
            item.city_name_it = getTranslation("it", `${item.city_name}`).toLocaleLowerCase();
            return item;
        });




        fs.writeFile(`${process.cwd()}/src/data/cities.json`, JSON.stringify(allCitiesArray), 'utf8', function () {
            console.log("Cities data updated");
        });

    } catch (error) {
        return [];
    }
}

