import axios from 'axios';
var fs = require('fs');
import  IAggregatorGeoNodes from "../Interfaces/IAggregatorGeoNodes";
import {getTranslation} from "../translations/translator";
import {IDictionaryItems} from "../Interfaces/IDictionaryItem";

export async function getProfileServices(lang: string, profileId: number) {
    try {
        const query: Object = `query profileByRef {
  public {
    profileByRef(id: ${profileId}) {
      showname
      provided_services {
        id
        type
        currency
        surcharge_price
        value
      }
    }
  }
}`;


        const response = await axios.post("https://api.escortforumit.xxx/graphql", {
            query: query,
        });


        return response.data.data.public.profileByRef.provided_services;

    } catch (error) {
        return [];
    }
}

