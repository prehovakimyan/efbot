import axios from 'axios';
var fs = require('fs');
import  IAggregatorGeoNodes from "../Interfaces/IAggregatorGeoNodes";
import {getTranslation} from "../translations/translator";
import {IDictionaryItems} from "../Interfaces/IDictionaryItem";

export async function getDictionaries() {
    try {

        const query: Object = `query dictionaryList {
                                      public {
                                        dictionaryList(categories: [EXTERIOR, OLD_EF, SYSTEM, ITALY_GEOGRAPHY]) {
                                          key
                                          category
                                          value_en
                                          value_it
                                          slug_en
                                          slug_it
                                        }
                                      }
                                    }`;


        const response = await axios.post("https://api.escortforumit.xxx/graphql", {
            query: query,
        });

        const dictionaries: IDictionaryItems = response.data.data.public.dictionaryList;
        fs.writeFile(`${process.cwd()}/src/translations/dictionary.json`, JSON.stringify(dictionaries), 'utf8', function () {});
        return true;

    } catch (error) {
        console.log(error);
        return false;
    }
}

