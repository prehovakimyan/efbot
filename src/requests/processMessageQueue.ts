export async function processMessageQueue(ctx: any, lang: string, messageQueue: any) {
    try {

        if (messageQueue.length > 0) {
            const message = messageQueue[0]; // Get the first message in the queue

            if(message.isProfile) {
                ctx.replyWithPhoto({ url: message.msgBody.url }, {caption: message.msgBody.caption, parse_mode: "HTML", reply_markup: message.msgBody.keyboard }).then(() => {
                    messageQueue.shift();
                    processMessageQueue(ctx, lang, messageQueue);
                });
            } else {
                ctx.reply( message.text, message.keyboard);
            }

        }

    } catch (error) {
        return [];
    }
}

