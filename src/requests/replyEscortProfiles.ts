import {getProfileServices} from "./getProfileServices";
import {getTranslation} from "../translations/translator";

export async function replyEscortProfiles(ctx: any, lang: string, escortsList: any) {
    try {
        var replyMessageProfiles: Array<any> = [];
        if(escortsList.length > 0) {
            escortsList.map(async (item: any, index:number) => {

                const profileServices = await getProfileServices(lang, item.id);

                var services: string = '';

                if(profileServices.length > 1) {
                    services = `<b style="font-size: 28px">🔥 ${getTranslation(lang, 'lbl_f_services')}</b>`;
                    profileServices.forEach((item: any, index:any) => {
                        if(index < 5) {
                            services += `✅ ${getTranslation(lang, item.value.toLowerCase()).trim()} \n`;
                        }
                    })
                }

                var phone: string = '';
                if(item.phone_number) {
                    phone = `<b>📱 ${getTranslation(lang, 'phone')}:</b> +${item.phone_country_code + item.phone_number.slice(0, 3)}... \n`;
                }
                var ageTxt: string = '';
                if(item.age) {
                    ageTxt = `<b>🔞 ${getTranslation(lang, 'age')}:</b> ${item.age}  \n`;
                }

                const photoUrl = "https://i.escortforumit.xxx/" + item.photo_path + '/'+ item.photo_hash + "_profile."+ item.photo_ext;
                const caption = `<b>💋 ${getTranslation(lang, 'lbl_name')}:</b> ${item.showname} \n${ageTxt}${phone}${services}`;
                const url = `https://www.escortforumit.xxx/accompagnatrici/${item.showname}-${item.id}`;



                const keyboard = {
                    inline_keyboard: [
                        [{ text: `${getTranslation(lang, 'show_more')}`, url: url }]
                    ],
                    resize_keyboard: true
                };

                var replyMessageRow = {
                    url: photoUrl,
                    caption: caption,
                    keyboard: keyboard
                };

                replyMessageProfiles.push(replyMessageRow);

                ctx.replyWithPhoto({ url: photoUrl }, {caption: caption, parse_mode: "HTML", reply_markup: keyboard });
            });
        }
        return replyMessageProfiles;
    } catch (error) {
        return [];
    }
}

