import axios from 'axios';
import  IAggregatorGeoNodes from "../Interfaces/IAggregatorGeoNodes";
import {getTranslation} from "../translations/translator";
import ICitiesRows from "../Interfaces/ICitiesRows";
import {getProfileServices} from "./getProfileServices";

export async function getProfileForJobSchedule(cityName: string, page: number, lang: string = "it") {
    try {

        const query: Object = `query aggregatorProfiles {
  public {
    aggregatorProfiles(
      geo: { type: CITY city_name: "${cityName.toLocaleLowerCase()}" }
      categories: {
        regular: true
      }
      filters: {
        is_telegram_bot_enabled: true
      }
      search: {}
      limit: 10
      offset: 0
      ordering: DEFAULT
    ) {
      count
      result {
         id
        profile_id
        showname
        photo_v
        photo_hash
        photo_ext
        photo_path
        phone_number
        age
        phone_country_code
      }
    }
  }
}`;




        const response = await axios.post("https://api.escortforumit.xxx/graphql", {
            query: query,
        });

        var result = response.data.data.public.aggregatorProfiles;


        await Promise.all(result.result.map(async (item: any, index:number) => {

            const serviceQuery: Object = `query profileByRef {
                                                  public {
                                                    profileByRef(id: ${item.id}) {
                                                      showname
                                                      provided_services {
                                                        id
                                                        type
                                                        currency
                                                        surcharge_price
                                                        value
                                                      }
                                                    }
                                                  }
                                                }`;

            const responseService = await axios.post("https://api.escortforumit.xxx/graphql", {
                query: serviceQuery,
            });



            const profileServices = responseService.data.data.public.profileByRef.provided_services;

            var services: string = '';

            if(profileServices.length > 1) {
                services = `<b style="font-size: 28px">🔥 ${getTranslation(lang, 'lbl_f_services')}</b>`;
                profileServices.forEach((item: any, index:any) => {
                    if(index < 5) {
                        services += `✅ ${getTranslation(lang, item.value.toLowerCase()).trim()} \n`;
                    }
                })
            }

            var phone: string = '';
            if(item.phone_number) {
                phone = `<b>📱 ${getTranslation(lang, 'phone')}:</b> +${item.phone_country_code + item.phone_number.slice(0, 3)}... \n`;
            }
            var ageTxt: string = '';
            if(item.age) {
                ageTxt = `<b>🔞 ${getTranslation(lang, 'age')}:</b> ${item.age}  \n`;
            }

            const photoUrl = "https://i.escortforumit.xxx/" + item.photo_path + '/'+ item.photo_hash + "_profile."+ item.photo_ext;
            const caption = `<b>💋 ${getTranslation(lang, 'lbl_name')}:</b> ${item.showname} \n${ageTxt}${phone}${services}`;
            const url = `https://www.escortforumit.xxx/accompagnatrici/${item.showname}-${item.id}`;



            const keyboard = {
                inline_keyboard: [
                    [{ text: `${getTranslation(lang, 'verify_learn_more')}`, url: url }]
                ],
                resize_keyboard: true
            };

            var replyMessageRow = {
                url: photoUrl,
                caption: caption,
                keyboard: keyboard
            };

            item.msgBody = replyMessageRow;
            item.isProfile = true;

        }));


        return result.result[(Math.floor(Math.random() * result.result.length))];

    } catch (error) {
        return [];
    }
}

