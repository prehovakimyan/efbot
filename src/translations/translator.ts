import fs from 'fs';
import {IDictionaryItem, IDictionaryItems} from "../Interfaces/IDictionaryItem";

interface TranslationData {
    [key: string]: string;
}

export function getTranslation(language: string, key: string): string {
    const translationPath = `${process.cwd()}/src/translations/${language}.json`;
    const dictionaryPath = `${process.cwd()}/src/translations/dictionary.json`;

    try {
        const dataTranslations = fs.readFileSync(translationPath, 'utf8');
        const dataDictionary = fs.readFileSync(dictionaryPath, 'utf8');
        const translations: TranslationData = JSON.parse(dataTranslations);
        const dictionaries: IDictionaryItems = JSON.parse(dataDictionary);
        let item: any = dictionaries.find((o: any) => o['key'] === key);

        if(item) {
            let keyStr: string = `${'value_'+ language}`;
            return item[keyStr];
        }
        else if(translations[key] ) {
            return translations[key];
        }
        return '';
    } catch (error) {
        console.error(`Failed to load translation file for language ${language}`);
        return '';
    }
}