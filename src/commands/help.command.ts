import {Command} from "./command.class";
import {Context, Telegraf} from "telegraf";
import {IBotContext} from "../context/context.interface";
import  IAggregatorGeoNodes from "../Interfaces/IAggregatorGeoNodes";
import "reflect-metadata";
import {getNearCities} from "../requests/getNearCities";
import { getTranslation } from '../translations/translator';
import ICitiesRows from "../Interfaces/ICitiesRows";

export class HelpCommand extends Command {

    protected lang: string = 'it';

    constructor(bot: Telegraf<IBotContext>) {
        super(bot);
    }


    handle(): void {
        this.bot.help(async (ctx) => {

            if(ctx.session.language) {
                this.lang = ctx.session.language;
            }

            // @ts-ignore
            ctx.reply(getTranslation(this.lang, 'helpTxt'));
        });
    }
}