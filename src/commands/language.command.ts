import {Command} from "./command.class";
import {Context, Telegraf} from "telegraf";
import {IBotContext} from "../context/context.interface";
import "reflect-metadata";
import { getTranslation } from '../translations/translator';
import {getNearCities} from "../requests/getNearCities";



export class LanguageCommand extends Command {

    protected lang: string = 'it';

    constructor(bot: Telegraf<IBotContext>) {
        super(bot);
    }

    handle(): void {


        this.bot.on("text",  async (ctx) => {

            if(ctx.session.language) {
                this.lang = ctx.session.language;
            }

            if(ctx.message.text == getTranslation(this.lang, 'change_language')) {
             var selectedLang = ctx.session.language = (this.lang == 'en') ? "it" : "en";

             if(ctx.session.latitude) {
                 const nearCities = await getNearCities(ctx.session.latitude, ctx.session.longitude , selectedLang, ctx);

                 // @ts-ignore
                 ctx.reply(getTranslation(selectedLang, 'choose_location'), {
                     reply_markup: {
                         keyboard: nearCities,
                         resize_keyboard: true
                     },
                 });
             } else {

                 // @ts-ignore
                 await ctx.reply(`${getTranslation(selectedLang, 'share_location_msg')}`, {
                     reply_markup: {
                         keyboard: [
                             [
                                 {
                                     text: `${getTranslation(selectedLang, 'popular_cities')}`
                                 },
                                 {
                                     text: `${getTranslation(selectedLang, 'share_location_btn')}`,
                                     request_location: true,
                                 },
                             ],
                             [ { text: `${getTranslation(selectedLang, 'change_language')}`} ],
                         ],
                         resize_keyboard: true,
                         one_time_keyboard: true,
                     },
                 });
             }

            }

        });

    }
}