import {Command} from "./command.class";
import {Context, Telegraf} from "telegraf";
import {ConfigService} from "../config/config.service";
import {IConfigService} from "../config/config.interface";
import {IBotContext} from "../context/context.interface";
import "reflect-metadata";
import { getTranslation } from '../translations/translator';
import {getMoreCities} from "../requests/getMoreCities";
import {getNearCities} from "../requests/getNearCities";
import {getProfilesByCityName} from "../requests/getProfilesByCityName";
import {getProfileServices} from "../requests/getProfileServices";
import {getProfilesByLocation} from "../requests/getProfilesByLocation";
import {getPopularCities} from "../requests/getPopularCities";
import {replyEscortProfiles} from "../requests/replyEscortProfiles";
import {processMessageQueue} from "../requests/processMessageQueue";
import {isCity} from "../middleware/isCity";

import {setTimeout} from "timers";

export class ActionsCommands extends Command {

    protected lang: string = 'it';
    protected messageQueue: Array<any> = [];

    constructor(bot: Telegraf<IBotContext>) {
        super(bot);
    }

    handle(): void {


        this.bot.on("text",  async (ctx) => {
            if(ctx.session.language) {
                this.lang = ctx.session.language;
            }

            switch (ctx.message.text) {

                case getTranslation(this.lang, 'more_cities'): {

                    const nextPage = (ctx.session.nextPage) ? ctx.session.nextPage : 2;
                    const nearCities = await getMoreCities(ctx.session.latitude, ctx.session.longitude, this.lang, ctx, nextPage);

                    // @ts-ignore
                    ctx.reply(getTranslation(this.lang, 'choose_location'), {
                        reply_markup: {
                            keyboard: nearCities,
                            resize_keyboard: true
                        },
                    });

                    break;

                }
                case getTranslation(this.lang, 'change_language'):
                case "/changelanguage": {
                    var selectedLang = ctx.session.language = (this.lang == 'en') ? "it" : "en";

                    if(ctx.session.latitude) {
                        const nearCities = await getNearCities(ctx.session.latitude, ctx.session.longitude, selectedLang, ctx);

                        // @ts-ignore
                        ctx.reply(getTranslation(selectedLang, 'choose_location'), {
                            reply_markup: {
                                keyboard: nearCities,
                                resize_keyboard: true
                            },
                        });
                    } else {

                        // @ts-ignore
                        await ctx.reply(`${getTranslation(selectedLang, 'share_location_msg')}`, {
                            reply_markup: {
                                keyboard: [
                                    [
                                        {
                                            text: `${getTranslation(selectedLang, 'popular_cities')}`
                                        },
                                        {
                                            text: `${getTranslation(selectedLang, 'share_location_btn')}`,
                                            request_location: true,
                                        },
                                    ],
                                    [ { text: `${getTranslation(selectedLang, 'change_language')}`} ],
                                ],
                                resize_keyboard: true,
                                one_time_keyboard: true,
                            },
                        });
                    }
                    break
                }
                case getTranslation(this.lang, 'change_location'): {
                    var nearCities = await getPopularCities(this.lang, ctx);

                    if(ctx.session.latitude) {
                        nearCities = await getNearCities(ctx.session.latitude, ctx.session.longitude , this.lang, ctx);
                    }
                    // @ts-ignore
                    ctx.reply(getTranslation(this.lang, 'choose_location'), {
                        parse_mode: "HTML",
                        reply_markup: {
                            keyboard: nearCities,
                            resize_keyboard: true
                        },
                    });
                    break
                }
                case getTranslation(this.lang, 'current_location'): {
                    ctx.session.currentCity = getTranslation(this.lang, 'current_location');
                    const result = await getProfilesByLocation(ctx.session.latitude, ctx.session.longitude, 0, this.lang);
                    const processMessagesList = result.result;
                    var show_more_keyword_row: any = [{ text: `${getTranslation(this.lang, 'change_location')}`,  }];
                    if(result.count > 5) {
                        show_more_keyword_row.push({ text: `${getTranslation(this.lang, 'lbl_more_profile')}`,  });
                    }
                    var show_more_keywords = [
                        show_more_keyword_row
                    ];

                    const aftherProfilesMsg = {
                        text: getTranslation(this.lang, 'show_more_msg'),
                        keyboard: {
                            parse_mode: "HTML",
                            reply_markup: {
                                keyboard: show_more_keywords,
                                resize_keyboard: true
                            }
                        }
                    };

                    processMessagesList.push(aftherProfilesMsg);
                    processMessageQueue(ctx, this.lang, processMessagesList);

                    break
                }
                case getTranslation(this.lang, 'lbl_more_profile'): {
                    var result;
                    if(ctx.session.currentCity == getTranslation(this.lang, 'current_location')) {
                        result = await getProfilesByLocation(ctx.session.latitude, ctx.session.longitude, ctx.session.showMorePage);
                    } else {
                        result = await getProfilesByCityName(ctx.session.currentCity,  ctx.session.showMorePage, this.lang);
                    }

                    const processMessagesList = result.result;

                    var show_more_keyword_row: any = [{ text: `${getTranslation(this.lang, 'change_location')}`,  }];

                    var showMoreMsg = getTranslation(this.lang, 'show_more_msg');
                    let redirectToWeb = false;
                    if(result.count > 5 && (ctx.session.showMorePage + 1) * 5 < result.count) {
                        if(ctx.session.showMorePage == 3){
                            showMoreMsg = getTranslation(this.lang, 'redirect_to_web');
                        }
                        show_more_keyword_row.push({ text: `${getTranslation(this.lang, 'lbl_more_profile')}`,  });
                    } else {
                        showMoreMsg = getTranslation(this.lang, 'redirect_to_web');
                    }

                    var show_more_keywords = [
                        show_more_keyword_row
                    ];

                    ctx.session.showMorePage += 1;

                    const aftherProfilesMsg = {
                        text: showMoreMsg,
                        keyboard: {
                            parse_mode: "HTML",
                            reply_markup: {
                                keyboard: show_more_keywords,
                                resize_keyboard: true
                            }
                        }
                    };

                    processMessagesList.push(aftherProfilesMsg);
                    processMessageQueue(ctx, this.lang, processMessagesList);
                    break;
                }
                case getTranslation(this.lang, 'popular_cities'): {
                    const nearCities = await getPopularCities(this.lang, ctx);

                    // @ts-ignore
                    ctx.reply(getTranslation(this.lang, 'choose_location'), {
                        parse_mode: "HTML",
                        reply_markup: {
                            keyboard: nearCities,
                            resize_keyboard: true
                        },
                    });
                    break;
                }
                default: {
                    ctx.session.currentCity = ctx.message.text;
                    ctx.session.showMorePage = 1;

                    const checkCityName: any = await isCity(ctx.message.text);

                    if(checkCityName) {
                        const result = await getProfilesByCityName(checkCityName,  0, this.lang);
                        var processMessagesList = result.result;

                        var show_more_keyword_row: any = [{ text: `${getTranslation(this.lang, 'change_location')}`,  }];

                        var showMoreMsg = getTranslation(this.lang, 'show_more_msg');

                        if(result.count > 5 && ctx.session.showMorePage * 5 < result.count) {
                            if(ctx.session.showMorePage == 3){
                                showMoreMsg = getTranslation(this.lang, 'redirect_to_web');
                            }
                            show_more_keyword_row.push({ text: `${getTranslation(this.lang, 'lbl_more_profile')}`,  });
                        } else {
                            showMoreMsg = getTranslation(this.lang, 'redirect_to_web');
                        }

                        var show_more_keywords = [
                            show_more_keyword_row
                        ];

                        if(result.count == 0) {
                            showMoreMsg = getTranslation(this.lang, 'no_escorts_msg_redirect');
                        }

                        const aftherProfilesMsg = {
                            text: showMoreMsg,
                            keyboard: {
                                parse_mode: "HTML",
                                reply_markup: {
                                    keyboard: show_more_keywords,
                                    resize_keyboard: true
                                }
                            }
                        };

                        processMessagesList.push(aftherProfilesMsg);
                        processMessageQueue(ctx, this.lang, processMessagesList);
                    } else {
                        ctx.reply(getTranslation(this.lang, 'lbl_no_city'), {
                            parse_mode: "HTML",
                            reply_markup: {
                                keyboard: [[{ text: `${getTranslation(this.lang, 'change_language')}`}, { text: `${getTranslation(this.lang, 'change_location')}`,  }]],
                                resize_keyboard: true
                            },
                        });
                    }

                    break;

                }
            }

        });

    }
}