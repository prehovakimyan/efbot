import {Command} from "./command.class";
import {Context, Telegraf} from "telegraf";
import {IBotContext} from "../context/context.interface";
import  IAggregatorGeoNodes from "../Interfaces/IAggregatorGeoNodes";
import "reflect-metadata";
import {getDictionaries} from "../requests/getDictionaries";
import {getNearCities} from "../requests/getNearCities";
import { getTranslation } from '../translations/translator';
import ICitiesRows from "../Interfaces/ICitiesRows";

export class StartCommand extends Command {

    protected lang: string = 'it';

    constructor(bot: Telegraf<IBotContext>) {
        super(bot);
    }


    handle(): void {
        this.bot.start(async (ctx) => {

            if(ctx.session.language) {
                this.lang = ctx.session.language;
            }

            if(!ctx.session.getLocation){
                // @ts-ignore
                await ctx.reply(`${getTranslation(this.lang, 'share_location_msg')}`, {
                    reply_markup: {
                        keyboard: [
                            [
                                {
                                    text: `${getTranslation(this.lang, 'popular_cities')}`
                                },
                                {
                                    text: `${getTranslation(this.lang, 'share_location_btn')}`,
                                    request_location: true,
                                },
                            ],
                            [ { text: `${getTranslation(this.lang, 'change_language')}`} ],
                        ],
                        resize_keyboard: true,
                        one_time_keyboard: true,
                    },
                });
            } else {
                const nearCities = await getNearCities(ctx.session.latitude, ctx.session.longitude , this.lang, ctx);
                // @ts-ignore
                ctx.reply(getTranslation(this.lang, 'choose_location'), {
                    parse_mode: "HTML",
                    reply_markup: {
                        keyboard: nearCities,
                        resize_keyboard: true
                    },
                });
            }
        });

        this.bot.on('location', async (ctx) => {
            const latitude = ctx.message.location.latitude;
            const longitude = ctx.message.location.longitude;

            ctx.session.getLocation = true;
            ctx.session.latitude = latitude;
            ctx.session.longitude = longitude;

            const nearCities = await getNearCities(latitude, longitude , this.lang, ctx);

            // @ts-ignore
            ctx.reply(getTranslation(this.lang, 'choose_location'), {
                reply_markup: {
                    keyboard: nearCities
                },
            });

        });

    }
}