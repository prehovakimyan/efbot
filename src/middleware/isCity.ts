import fs from 'fs';
import IAllCities from "../Interfaces/IAllCities";

export async function isCity(checkString: string) {
    const allCitiesJsonFile = `${process.cwd()}/src/data/cities.json`;
    const allCitiesJson = fs.readFileSync(allCitiesJsonFile, 'utf8');
    try {
        const allCities: IAllCities = JSON.parse(allCitiesJson);
        let item: any = allCities.find((o: any) => {
           return  o['city_name_it'].toLocaleLowerCase() == checkString.toLocaleLowerCase() || o['city_name_en'].toLocaleLowerCase() == checkString.toLocaleLowerCase() ;
        });

        if(item) {
            return item.city_name;
        }

        return false;

    } catch (error) {
        return false;
    }
}

