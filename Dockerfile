# Use an official Node.js runtime as the base image
FROM node:16.17.0

# Set the working directory inside the container
WORKDIR /var/www/

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code to the working directory
COPY . .

# Install PM2 globally
RUN npm install pm2 -g

# Expose the port your application will run on
EXPOSE 3000

# Start your application using PM2
CMD ["pm2-runtime", "start", "npm", "--", "start"]

